@extends('layouts.app')

@section('content')
    <div class="container" id="movie-app">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Order History
                    </div>
                    <div class="panel-body">
                        @if(!empty($orders))
                            <table class="table">
                                <tr>
                                    <th>
                                        Order Reference
                                    </th>
                                    <th>
                                        Details
                                    </th>
                                    <th>
                                        Order Date
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                @foreach($orders as $order)
                                    <tr class="active">
                                        <td>
                                            {{ $order->reference }}
                                        </td>
                                        <td>
                                            <strong>Tickets:</strong>
                                            <span v-if="bookings[{{{ $order->id }}}]">@{{ bookings[{{{ $order->id }}}].length }}</span>
                                            <span v-else>{{ $order->total_bookings }}</span>
                                            <br />
                                            <strong>Cinema:</strong> {{ $order->cinema_name }}<br />
                                            <strong>Theatre:</strong>  {{ $order->theatre_name }}<br />
                                            <strong>Movie:</strong>  {{ $order->movie_name }}<br />
                                            <strong>Show time:</strong>  {{ $order->show_time }}<br />
                                            <strong>Total:</strong>
                                            <span v-if="bookings[{{{ $order->id }}}]">R@{{ bookings[{{{ $order->id }}}].length *  bookings[{{{ $order->id }}}][0].price}}</span>
                                            <span v-else>R{{ $order->total }}</span>
                                            <br />
                                        </td>
                                        <td>
                                            {{ $order->created_at }}
                                        </td>
                                        <td>
                                            <form id="order_form_{{ $order->id }}" method="POST" action="/order-history/{{ $order->id }}" v-on:submit="validateOrderDelete({{{ $order->id }}}, $event)">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="javascript:void(0);" v-on:click="getBookings({{ $order->id }})" class="btn btn-sm btn-info">View</a>
                                                <button class="btn btn-sm btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr v-if="bookings[{{{ $order->id }}}]">
                                        <td colspan="4">
                                            <table class="table">
                                                <tr>
                                                    <th>
                                                        Ticket reference
                                                    </th>
                                                </tr>
                                                <tr v-for="booking in bookings[{{{ $order->id }}}]" :key="booking.id">
                                                    <td>
                                                        @{{ booking.reference }}
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm btn-danger" v-on:click="deleteBooking(booking.id)">Delete</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
