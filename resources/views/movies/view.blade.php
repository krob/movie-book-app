@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(!empty($movie))
                        {{ $movie->name }}
                        @if(!empty($cinema))
                            - {{ $cinema->name }}
                        @endif
                    @else
                        No movie found
                    @endif
                </div>

                <div class="panel-body">
                    @if(!empty($movie))
                        <div class="row">
                            <div class="col-sm-12 cinema-card text-center">
                                @if($movie->image != '')
                                    <img src="{{ $movie->image }}" alt="{{ $movie->name }}" />
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>{{ $movie->name }}</h4>
                                <div>
                                    {{ $movie->description }}
                                </div>
                                <h5>
                                    Duration: {{ gmdate('H:i', $movie->duration * 60) }}
                                </h5>
                            </div>
                        </div>
                        @if(!empty($showTimes))
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2>BOOK NOW</h2>
                                    <table class="table">
                                        <tr>
                                            <th>
                                                Show Times
                                            </th>
                                            <th>
                                                Theatre
                                            </th>
                                            <th>
                                                Availability
                                            </th>
                                            <th>
                                                Qty
                                            </th>
                                            <th>
                                                Price
                                            </th>
                                            <th></th>
                                        </tr>
                                        @foreach($showTimes as $showTime)
                                            <form method="post">
                                                {!! csrf_field() !!}
                                                <tr>
                                                    <td>
                                                        {{ $showTime->show_time }}
                                                    </td>
                                                    <td>
                                                        {{ $showTime->theatre_name }}
                                                    </td>
                                                    <td>
                                                        {{$showTime->available_tickets}}/{{$showTime->capacity}} tickets left
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="num_tickets">
                                                            @for($i=1; $i <= $showTime->available_tickets; $i++)
                                                                <option {{ (old("num_tickets") == $i ? "selected":"") }} value="{{ $i }}">
                                                                    {{ $i }}
                                                                </option>
                                                            @endfor
                                                        </select>
                                                    </td>
                                                    <td>
                                                        R{{  number_format($showTime->price, 2, '.', '') }}
                                                    </td>
                                                    <td>
                                                        @if($showTime->available_tickets < 1)
                                                            <a class="btn btn-danger" href="javascript:void(0);">Sold out</a>
                                                        @elseif(!Auth::check())
                                                            <a class="btn btn-info" href="/login">Login to book</a>
                                                        @else
                                                            <button class="btn btn-info">
                                                                Book now
                                                            </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <input type="hidden" name="show_time" value="{{ $showTime->id }}" />
                                            </form>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
