@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    OOPS....
                </div>

                <div class="panel-body">
                    <h4>{{ $message }}</h4>
                    Sorry, we cannot find that page.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
