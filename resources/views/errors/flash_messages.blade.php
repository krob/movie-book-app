
@if (session('error_status'))
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="alert alert-danger">
                    {!! session('error_status') !!}
                </div>
            </div>
        </div>
    </div>
@endif
@if (session('success_status'))
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="alert alert-success">
                    {!! session('success_status') !!}
                </div>
            </div>
        </div>
    </div>
@endif
