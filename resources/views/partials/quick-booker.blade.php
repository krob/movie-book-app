<h4>QUICK BOOKER</h4>
<div id="movie-app">
    <table class="table">
        <tr>
            <th>
                Cinema
            </th>
            <th colspan="6">
                Movie
            </th>
        </tr>
        <tr>
            <td>
                <select name="cinema" class="form-control" v-on:change="getMoviesByCinema">
                    <option value="">Select a cinema</option>
                    @foreach($cinemas as $cinema)
                        <option value="{{ $cinema->id }}">{{ $cinema->location }} - {{ $cinema->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select name="movie" class="form-control" v-on:change="getMovieAvailableDates">
                    <option value="">Select a movie</option>
                    <option v-bind:value="movie.movie_id" v-for="movie in quick_book.movies">
                        @{{ movie.movie_name }}
                    </option>
                </select>
            </td>
            <!-- <td>
                <select name="date_time" class="form-control">
                    <option value="">Select a date/time</option>
                    <option v-for="movie in quick_book.movies">
                        @{{ movie.movie_name }}
                    </option>
                </select>
            </td>
            <td>
                <select name="qty" class="form-control">
                    <option value="">Number of tickets</option>

                </select>
            </td>
            <td>R@{{ quick_book.total }}</td> -->

        </tr>
        <tr>
            <th>
                Show Times
            </th>
            <th>
                Theatre
            </th>
            <th>
                Availability
            </th>
            <th>
                Qty
            </th>
            <th>
                Price
            </th>
            <th></th>
        </tr>
        <tr v-for="movie_data in quick_book.movie_data.showTimes" :key="movie_data.id">
            <td>
                @{{ movie_data.show_time }}
            </td>
            <td>
                @{{ movie_data.theatre_name }}
            </td>
            <td>
                @{{movie_data.available_tickets}}/@{{movie_data.capacity}} tickets left
            </td>
            <td>
                <select name="qty" class="form-control" id="qty_@{{ movie_data.id }}">
                    <option  v-bind:value="n+1" v-for="n in movie_data.available_tickets">@{{ n+1 }}</option>
                </select>
            </td>
            <td>
                R@{{ movie_data.price }}
            </td>
            <!-- <td>
                <span v-if="quick_book.total > 0">R@{{ quick_book.total }}</span>
                <span v-else>R@{{ movie_data.price }}</span>
            </td> -->
            <td align="right">
                @if(!Auth::check())
                    <a v-if="movie_data.available_tickets < 1" class="btn btn-danger" href="javascript:void(0);">Sold out</a>
                    <a v-else class="btn btn-info" href="/login">Login to book</a>
                @else
                    <a v-if="movie_data.available_tickets < 1" class="btn btn-danger" href="javascript:void(0);">Sold out</a>
                    <button v-else class="btn btn-info" v-on:click="bookMovie(movie_data.id)">Book now</button>
                @endif
            </td>
        </tr>
    </table>
</div>
