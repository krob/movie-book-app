@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>
                @if(!empty($cinemas))
                    <div class="panel-body">
                        <div class="row">
                            @foreach($cinemas as $cinema)
                                @if($cinema->image != '')
                                    <div class="col-sm-3 cinema-card">
                                        <a href="/cinema/{{ $cinema->id }}">
                                            <img src="{{ $cinema->image }}" alt="{{ $cinema->location }} - {{ $cinema->name }}" />
                                        </a>
                                        <div class="title">
                                            <h5>{{ $cinema->name }}</h5>
                                            {{ $cinema->location }}
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="panel-footer">
                        @include('partials.quick-booker')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
