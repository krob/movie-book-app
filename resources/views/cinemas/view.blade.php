@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(!empty($cinema))
                        {{ $cinema->name }} - {{ $cinema->location }}
                    @else
                        No cinema found
                    @endif
                </div>

                <div class="panel-body">
                    @if(!empty($cinema))
                        <div class="row">
                            <div class="col-sm-12 cinema-card text-center">
                                @if($cinema->image != '')
                                    <img src="{{ $cinema->image }}" alt="{{ $cinema->location }} - {{ $cinema->name }}" />
                                @endif
                            </div>
                        </div>
                        @if(!empty($movies))
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <br />
                                        @if(!isset($view_all))
                                        <a href="/cinema/{{ $cinema->id }}/movies">View all movies for {{ $cinema->location }}</a><br /><br />
                                    @endif
                                </div>
                                @foreach($movies as $movie)
                                    <div class="col-sm-3">
                                        <a href="/cinema/{{ $cinema->id }}/movie/{{ $movie->movie_id }}">
                                            @if($movie->movie_image != '')
                                                <img src="{{ $movie->movie_image }}" alt="{{ $movie->movie_name }}" />
                                            @endif
                                        </a>
                                        <div class="title">
                                            <h5>{{ $movie->movie_name }}</h5>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <h3>There are currently no movies showing at this Cinema.</h3>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
