'use strict';

if(document.getElementById("movie-app")){
    new Vue({
        http: {
            root: '/'
        },
        el: '#movie-app',
        data: {
            bookings: [],
            quick_book: {
                active:{
                    cinema_id: null,
                    theatre_id: null,
                    movie_id: null,
                    show_time_id: null
                },
                movies:[],
                movie_data:[],
                total : 0
            }
        },
        methods: {
            getBookings: function(orderid){
                var that = this;
                this.$http.get('/order-history/'+orderid).then(function(bookingsData){
                    that.$set('bookings['+orderid+']', bookingsData.data);
                });
            },
            deleteBooking: function(bookingid){
                var _token =  this.getXsrfToken();
                var that = this;
                var confirm_del = confirm("Are you sure you want to delete this booking?");
                if(!confirm_del){
                    return false;
                }
                Vue.http.headers.common['X-XSRF-TOKEN'] = _token;
                this.$http.delete('/booking/'+bookingid).then(function(bookingsData){
                    if(bookingsData.data.type == 'error'){
                        alert(bookingsData.data.message);
                        return false;
                    }
                    alert(bookingsData.data.message);
                    if(bookingsData.data.number_of_orders === 1){
                        window.location.reload();
                    }
                    that.getBookings(bookingsData.data.order_id);
                });
            },
            bookMovie: function(show_time_id){
                var _token =  this.getXsrfToken();
                var cinemaid = this.quick_book.active.cinema_id;
                var movieid = this.quick_book.active.movie_id;
                var num_tickets = document.getElementById("qty_"+show_time_id).value;
                Vue.http.headers.common['X-XSRF-TOKEN'] = _token;
                var that = this;

                this.$http.post('/cinema/'+cinemaid+'/movie/'+movieid,{show_time: show_time_id,num_tickets: num_tickets},{emulateJSON: true}).then(function(bookingData){
                    console.log("bookingData",bookingData);
                    alert(bookingData.data.message);
                    that.$http.get('/cinema/'+cinemaid+'/movie/'+movieid).then(function(moviesData){
                        that.$set('quick_book.movie_data', moviesData.data);
                    });

                });
            },
            validateOrderDelete: function(orderid, e){
                e.preventDefault();

                var confirm_del = confirm("Are you sure? Your order and bookings will be deleted.");
                if(!confirm_del){
                    return false;
                }
                var form = document.getElementById("order_form_"+orderid);
                form.submit();
            },
            getXsrfToken: function() {
                var cookies = document.cookie.split(';');
                var token = '';
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].split('=');
                    if(cookie[0] == 'XSRF-TOKEN') {
                        token = decodeURIComponent(cookie[1]);
                    }
                }
                return token;
            },
            getMoviesByCinema: function(e){
                var cinemaid = e.target.value;
                if(!cinemaid){
                    this.resetCinema();
                    return false;
                }
                this.$set('quick_book.active.cinema_id', cinemaid);
                this.resetMovieData();
                var that = this;
                this.$http.get('/cinema/'+cinemaid+'/movies').then(function(moviesData){
                    that.$set('quick_book.movies', moviesData.data);
                });
            },
            getMovieAvailableDates: function(e){
                var movieid = e.target.value;
                if(!movieid){
                    this.resetMovies();
                    return false;
                }
                var cinemaid = this.quick_book.active.cinema_id;
                this.$set('quick_book.active.movie_id', movieid);
                var that = this;
                this.$http.get('/cinema/'+cinemaid+'/movie/'+movieid).then(function(moviesData){
                    that.$set('quick_book.movie_data', moviesData.data);
                });
            },
            resetCinema: function(){
                this.$set('quick_book.active.cinema_id', null);
                this.$set('quick_book.movies', []);
                this.resetMovies();
                this.resetMovieData();
            },
            resetMovies: function(){
                this.$set('quick_book.active.movie_id', null);
                this.resetMovieData();
            },
            resetMovieData: function(){
                this.$set('quick_book.movie_data', []);
            },
            updateTotal: function(e, unit_price){
                // console.log("e",e);
                // var qty = e.target.value;
                // this.$set('quick_book.total', unit_price*qty);
            }
        },
        ready: function() {
            // this.init();
        }
    });
}
