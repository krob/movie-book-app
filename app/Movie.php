<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'movies';


  public function theatres(){
    return $this->hasMany('App\Theatres');
  }

  /**
   * [Get all movies associated with with cinema]
   * @param  [int]  $cinemaID [id of cinema]
   * @param  integer $limit    [limit number of movies returned]
   */
  public static function getMoviesByCinema($cinemaID, $limit=0){
    $movies =   \DB::table('theatres')
                ->leftJoin('theatres_movies_pivot', 'theatres.id', '=', 'theatres_movies_pivot.theatre_id')
                ->leftJoin('movies', 'theatres_movies_pivot.movie_id', '=', 'movies.id')
                ->select('theatres.name as theatre_name', 'theatres.capacity', 'movies.id as movie_id', 'movies.name as movie_name', 'movies.image as movie_image', 'movies.duration as movies_duration', 'theatres_movies_pivot.show_time', 'theatres_movies_pivot.price')
                ->where('theatres.cinema_id', '=', $cinemaID)
                ->groupBy('movies.id');

    if($limit > 0){
       $movies->limit($limit);
    }
    return $movies->get();
  }

  /**
   * [Get single movie details associated with with cinema]
   * @param  [int]  $cinemaID [id of cinema]
   * @param  [int]  $movieID [id of movie]
   */
  public static function getMovieByCinema($cinemaID, $movieID){
    $movies =   \DB::table('theatres')
                ->leftJoin('theatres_movies_pivot', 'theatres.id', '=', 'theatres_movies_pivot.theatre_id')
                ->leftJoin('movies', 'theatres_movies_pivot.movie_id', '=', 'movies.id')
                ->select('theatres.name as theatre_name', 'theatres.capacity', 'theatres.id as theatre_id', 'movies.id as movie_id', 'movies.name as movie_name', 'movies.image as movie_image', 'movies.duration as movies_duration', 'theatres_movies_pivot.show_time', 'theatres_movies_pivot.price', 'theatres_movies_pivot.id')
                ->where('theatres.cinema_id', '=', $cinemaID)
                ->where('movies.id', '=', $movieID)
                ->where('theatres_movies_pivot.show_time', '>', strtotime('now'))
                ->orderBy('theatres_movies_pivot.show_time', 'asc');

    return $movies->get();
  }

}
