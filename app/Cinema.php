<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'cinemas';


   public function theatres()
   {
      return $this->hasMany('App\Theatre', 'cinema_id');
   }

}
