<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Theatre as Theatre;

class Order extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'orders';

   protected $fillable = ['user_id', 'show_time_id', 'reference'];

   public function user()
   {
     return $this->hasOne('App\User');
   }

   public function bookings()
   {
     return $this->hasOne('App\Booking');
   }

   /**
    * [get number of available tickets left for movie at specific show time]
    * @param  [int] $showTimeID [id of movie at specific show time]
    * @return [int] [number of available tickets]
    */
   public static function getAvailableTicketsForMovieShow($showTimeID){

      $theatreCapacity = Theatre::getTheatreShowTimeObject($showTimeID);

      // Get orders / bookings for that specific movie / show time
      $orders = \DB::table('orders')
                  ->leftJoin('bookings', 'orders.id', '=', 'bookings.order_id')
                  ->where('orders.show_time_id', '=', $showTimeID);

      $ordersCount = $orders->count();

      // Available Tickets
      $availableTickets = $theatreCapacity->capacity - $ordersCount;

      return $availableTickets;
   }

}
