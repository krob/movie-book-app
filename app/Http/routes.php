<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/cinemas', 'CinemasController@index');
Route::get('/cinema/{id}', 'CinemasController@view');
Route::get('/cinema/{id}/movies', 'MoviesController@movies');
Route::get('/cinema/{cinemaID}/movie/{movieID}', 'MoviesController@movie');

//Error routes
Route::get('/404', 'IndexController@notFound');

// Protect routes that a user needs to be logged in to access
Route::group(['middleware' => ['auth']], function(){
    Route::post('/cinema/{cinemaID}/movie/{movieID}', 'MoviesController@bookMovie');
    Route::delete('/booking/{bookingId}', 'OrdersController@deleteBooking');
    Route::get('/order-history', 'OrdersController@orderHistory');
    Route::get('/order-history/{orderid}', 'OrdersController@orderBookings');
    Route::delete('/order-history/{orderid}', 'OrdersController@deleteOrder');

});

Route::auth('/login');
