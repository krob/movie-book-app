<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Order as Order;
use App\Booking as Booking;

class OrdersController extends Controller{

   /**
   * Get order history for logged in user
   *
   * @return view
   */
   public function orderHistory(){
      //Get user order history joining relevant tables to get values from ids
      $orders =   Order::where('user_id', \Auth::user()->id)
                  ->leftJoin('bookings', 'orders.id', '=', 'bookings.order_id')
                  ->leftJoin('theatres_movies_pivot', 'orders.show_time_id', '=', 'theatres_movies_pivot.id')
                  ->leftJoin('theatres', 'theatres_movies_pivot.theatre_id', '=', 'theatres.id')
                  ->leftJoin('cinemas', 'theatres.cinema_id', '=', 'cinemas.id')
                  ->leftJoin('movies', 'theatres_movies_pivot.movie_id', '=', 'movies.id')
                  ->select('orders.id', 'orders.reference', 'orders.created_at', 'theatres_movies_pivot.show_time', 'theatres.name as theatre_name', 'movies.name as movie_name', \DB::raw('CONCAT(cinemas.name, " - ", cinemas.location) as cinema_name'), \DB::raw('SUM(theatres_movies_pivot.price) as total'), \DB::raw('COUNT(bookings.id) as total_bookings'))
                  ->orderBy('orders.created_at', 'desc')
                  ->groupBy('orders.id');

      return view('orders/order_history')->with('orders', $orders->get());
   }

   /**
   * [Get bookings / tickets associated with the order]
   * @param  Request $request
   * @param  int  $order_id [id of order]
   * @return \Illuminate\Http\Response
   */
   public function orderBookings(Request $request, $order_id){
      if($request->ajax()){
         $bookings = Booking::where('order_id', $order_id)
                     ->leftJoin('orders', 'bookings.order_id', '=', 'orders.id')
                     ->leftJoin('theatres_movies_pivot', 'orders.show_time_id', '=', 'theatres_movies_pivot.id')
                     ->select('bookings.id', '.bookings.reference', 'bookings.order_id', 'bookings.cinema_id', 'bookings.theatre_id', 'bookings.movie_id', 'theatres_movies_pivot.price as price')
                     ->get();

         return response()->json($bookings);
      }
   }

   public function deleteOrder($order_id){
      $order = Order::where('orders.id', $order_id)
                     ->leftJoin('theatres_movies_pivot', 'orders.show_time_id', '=', 'theatres_movies_pivot.id')
                     ->select('orders.id', 'theatres_movies_pivot.show_time as show_time');

      if(strtotime("-1 hours") > strtotime($order->first()->show_time)){
         return back()->with('error_status', 'You cannot cancel 1hour before the show.');
      }

      if($order->delete()){
         return back()->with('success_status', 'Order Deleted.');
      }else{
         return back()->with('error_status', 'Error.');
      }
   }

   /**
    * [delete a single booking]
    * @param  [int] $booking_id [id of booking]
    * @return [json]
    */
   public function deleteBooking($booking_id){
      $ret = [
         "type" => "error",
         "message" => "Unable to process request."
      ];
      // Find the number of bookings attached to order
      $bookings = Booking::where('b1.id', $booking_id)
                  ->leftJoin('bookings as b1', 'bookings.order_id', '=', 'b1.order_id')
                  ->leftJoin('orders', 'bookings.order_id', '=', 'orders.id')
                  ->leftJoin('theatres_movies_pivot', 'orders.show_time_id', '=', 'theatres_movies_pivot.id')
                  ->select('b1.id', 'b1.order_id', 'b1.created_at', \DB::raw('COUNT(bookings.order_id) as number_of_orders'), 'theatres_movies_pivot.show_time as show_time');

      $booking = $bookings->first();

      // Cannot delete a booking within 1hour of movie starting
      if(strtotime("-1 hours") > strtotime($booking->show_time)){
         $ret = [
            "type" => "error",
            "message" => "You cannot cancel 1hour before the show."
         ];
         return response()->json($ret);
      }


      //In the order only has one booking, delete order when deleting last booking
      if($booking->number_of_orders == 1){
         if(Order::find($booking->order_id)->delete()){
            $ret['type'] = 'success';
            $ret['message'] = 'Booking and order deleted';
            $ret['order_id'] = $booking->order_id;
            $ret['booking_id'] = $booking->id;
            $ret['number_of_orders'] = $booking->number_of_orders;
         }
      }else{
         // delete the booking
         $booking->delete();
         $ret['type'] = 'success';
         $ret['message'] = 'Booking deleted';
         $ret['order_id'] = $booking->order_id;
         $ret['booking_id'] = $booking->id;
         $ret['number_of_orders'] = $booking->number_of_orders;
      }

      return response()->json($ret);
   }
}
