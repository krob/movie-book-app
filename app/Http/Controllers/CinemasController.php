<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Cinema as Cinema;
use App\Movie as Movie;
use App\Order as Order;
use App\Booking as Booking;

class CinemasController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * show all cinemas
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $cinemas = Cinema::orderBy('location')->get();
        return view('cinemas/index')->with('cinemas', $cinemas);
    }

    /**
     * [show movies for specific cinema]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function view($id){
        $cinema = Cinema::find($id);
        $movies = Movie::getMoviesByCinema($id, 4);
        return view('cinemas/view')->with(['cinema' => $cinema, 'movies' => $movies]);
    }
}
