<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Cinema as Cinema;

class IndexController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the available cinemas.
     */
    public function index()
    {
        $cinemas = Cinema::orderBy('location')->get();
        return view('welcome')->with('cinemas', $cinemas);
    }

    /**
     * [404 page]
     * @return 404 view
     */
    public function notFound(){
        $messages = ['Man overboard', 'Gone fishing', 'Looks like we\'re a little lost'];
        $message_index = array_rand($messages, 1);

        return view('errors.404')->with('message', $messages[$message_index]);
    }
}
