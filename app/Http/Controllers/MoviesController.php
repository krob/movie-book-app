<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Cinema as Cinema;
use App\Movie as Movie;
use App\Order as Order;
use App\Booking as Booking;

class MoviesController extends Controller{

   /**
    * [Get movies by cimena]
    * @param  Request $request
    * @param  [int]  $cinemaid [id of cinema]
    * @return if !ajax [view] with cinema obj and associated movies obj
    * @return if ajax [json] with associated movies obj
    **/
   public function movies(Request $request, $cinemaid){
      $cinema = Cinema::find($cinemaid);
      $movies = Movie::getMoviesByCinema($cinemaid);
      if($request->ajax()){
         return response()->json($movies);
      }else{
         return view('cinemas/view')->with(['cinema' => $cinema, 'movies' => $movies, 'view_all' => true]);
      }
   }

   /**
   * [Gte selected movie and associated show times]
   * @param  Request $request
   * @param  [int]  $cinemaID [id of cinema]
   * @param  [int]  $movieID  [id of movie]
   * @return if !ajax [view] with cinema obj, movie obj and show times of selected movie
   * @return if ajax [json] with cinema obj, movie obj and show times of selected movie
   */
   public function movie(Request $request, $cinemaID, $movieID){
      $cinema = Cinema::find($cinemaID);
      $showTimes = Movie::getMovieByCinema($cinemaID, $movieID);

       // Maps show times to get available seats left. *Must be a better way of doing this in SQL
      array_map( function($showTime) {
         $showTime->available_tickets = Order::getAvailableTicketsForMovieShow($showTime->id);
      }, $showTimes);

      $movie = Movie::find($movieID);
      if($request->ajax()){
         return response()->json(compact("cinema","showTimes","movie"));
      }else{
         return view('movies/view')->with(['cinema' => $cinema, 'showTimes' => $showTimes, 'movie' => $movie ]);
      }
   }

   /**
    * [Book a movie]
    * @param  Request $request
    * @param  [int]  $cinemaID [id of cinema]
    * @param  [int]  $movieID  [id of movie]
    * @return with message for saved booking
    */
   public function bookMovie(Request $request, $cinemaID, $movieID){

      $required = ["_token", "show_time", "num_tickets"];
      if($request->ajax()){
         // Token is sent in header, remove _token from check
         unset($required[0]);
      }

      //extra check to ensure valid params are being sent, if not respond with error
      foreach ($required as $value) {
         if(!array_key_exists($value, $_POST)){
            return back()->with('error_status', 'An error occurred. Invalid request.');
         }
      }

      $showTime = $_POST['show_time'];

      // Get avaiable tickets int
      $availableTickets = Order::getAvailableTicketsForMovieShow($showTime);

      // If tickets ordered are greater than tickets available, redirect with error
      if($_POST['num_tickets'] > $availableTickets){
         if($request->ajax()){
            return response()->json(['message'=>'There are <b>'.$availableTickets.'</b> available tickets left.']);
         }else{
            return back()->with('error_status', 'There are <b>'.$availableTickets.'</b> available tickets left.')->withInput();
         }
      }

      // Get Theatre ID
      $showTimeObject = \DB::table('theatres_movies_pivot')
                        ->select('show_time', 'theatre_id')
                        ->where('id', '=', $showTime)
                        ->first();

      // Create order
      $order =    Order::create([
                     'user_id' => \Auth::user()->id,
                     'show_time_id' => $showTime,
                     'reference' => sha1(time().str_random(40))
                  ]);

      for($i=1; $i<=$_POST['num_tickets']; $i++){
         $booking = new Booking();
         $booking->order_id = $order->id;
         $booking->cinema_id = $cinemaID;
         $booking->theatre_id = $showTimeObject->theatre_id;
         $booking->movie_id = $movieID;
         $booking->reference = sha1(time().$i.str_random(40));
         // Save the bookings for the order
         $booking->save();
      }

      if($request->ajax()){
         return response()->json(['message'=>'Booking saved. Go to your order history.']);
      }else{
         return back()->with('success_status', 'Booking saved. <a href="/order-history"><strong>Go to your order here</strong></a>.');
      }
   }

}
