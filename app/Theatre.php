<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theatre extends Model{
    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'theatres';


   public function movies(){
      return $this->hasMany('App\Movie');
   }

   /**
   * Get the Cinema that owns the theatre.
   */
   public function cinema(){
      return $this->belongsTo('App\Cinema');
   }

   /**
    * [get the specifics for a specific show time]
    * @param  [type] $showTimeID [if of show time from theatres_movies_pivot table]
    * @return [type]             [description]
    */
   public static function getTheatreShowTimeObject($showTimeID){

      $showTime = \DB::table('theatres_movies_pivot')
                     ->select('show_time', 'theatre_id')
                     ->where('id', '=', $showTimeID)
                     ->first();

      // Theatre capacity
      $theatre = Theatre::find($showTime->theatre_id);
      if(count($theatre) == 0){
         return [];
      }
      return $theatre;
   }

}
