<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheatresMoviesPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theatres_movies_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theatre_id')->unsigned()->index();
            $table->integer('movie_id')->unsigned()->index();
            $table->dateTime('show_time');
            $table->float('price');
            $table->timestamps();

            // Foreign key constraints
            $table->foreign('theatre_id')->references('id')->on('theatres')->onDelete('cascade');
            $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('theatres_movies_pivot', function (Blueprint $table) {
            //
        });
    }
}
