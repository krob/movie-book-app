<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheatresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theatres', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cinema_id')->unsigned()->index();
            $table->string('name')->index();
            $table->integer('capacity');
            $table->timestamps();

            // Foreign key constraints
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('theatres', function (Blueprint $table) {
            Schema::drop('theatres');
        });
    }
}
