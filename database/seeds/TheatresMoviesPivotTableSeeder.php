<?php

use Illuminate\Database\Seeder;

class TheatresMoviesPivotTableSeeder extends Seeder
{
    CONST MIN_PRICE = 30;
    CONST MAX_PRICE = 70;
    CONST MAX_ENTRIES = 10;
    CONST SHOW_TIMES = ['10:00','12:00','14:00','18:00','20:00','22:00'];

    public function __construct(){
        $this->theatresMoviesData = [];

        $this->theatreIDs = DB::table('theatres')->select('id')->get();
        $this->movieIDs = DB::table('movies')->select('id')->get();

        $this->usedTheatreIDs = [];
        $this->usedMovieIDs = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete data
        \DB::table('theatres_movies_pivot')->delete();

        // Insert data
        \DB::table('theatres_movies_pivot')->insert($this->generateTheatresMoviesData());
    }

    private function generateTheatresMoviesData(){
        for($i=0; $i<=self::MAX_ENTRIES; $i++){
            foreach (self::SHOW_TIMES as $showTime) {
                $date = new DateTime(date("Y-m-d ".$showTime));
                if($i > 0){
                    $date->modify('+'.$i.' day');
                }
                $data = [
                    'theatre_id' => $this->getTheatresById(),
                    'movie_id' => $this->getMoviesById(),
                    'show_time' => $date->format('Y-m-d H:i:s'),
                    'price' => mt_rand(self::MIN_PRICE,self::MAX_PRICE)
                ];
                $this->theatresMoviesData[] = $data;
            }
        }

        return $this->theatresMoviesData;
    }

    private function getTheatresById(){
        if(is_array($this->theatreIDs)){
            return $this->theatreIDs[array_rand($this->theatreIDs)]->id;
        }
    }

    private function getMoviesById(){
        if(is_array($this->movieIDs)){
            return $this->movieIDs[array_rand($this->movieIDs)]->id;
        }
    }

}
