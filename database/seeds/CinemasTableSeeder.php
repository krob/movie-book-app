<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete data
        \DB::table('cinemas')->delete();

        // Insert data
        \DB::table('cinemas')->insert([
            [
                'name' => 'Sterkinekor',
                'image' => 'https://dummyimage.com/300x300',
                'location' => 'Canal Walk'
            ],
            [
                'name' => 'Sterkinekor',
                'image' => 'https://dummyimage.com/300x300',
                'location' => 'Sandton City'
            ],
            [
                'name' => 'Nu Metro',
                'image' => 'https://dummyimage.com/300x300',
                'location' => 'Bedforview'
            ],
            [
                'name' => 'Nu Metro',
                'image' => 'https://dummyimage.com/300x300',
                'location' => 'Bayside Mall'
            ]
        ]);
    }
}
