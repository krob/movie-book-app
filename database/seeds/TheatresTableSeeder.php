<?php

use Illuminate\Database\Seeder;
use App\Cinema;

class TheatresTableSeeder extends Seeder
{
    CONST MIN_CAPACITY = 5;
    CONST MAX_CAPACITY = 30;

    public function __construct(){
        $this->cinemaIDs = DB::table('cinemas')->select('id')->get();
        $this->usedCinemaIDs = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete data
        \DB::table('theatres')->delete();

        // Insert data
        \DB::table('theatres')->insert([
            [
                'name' => 'Cine 1',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 2',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 3',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 4',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 5',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 6',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 7',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ],
            [
                'name' => 'Cine 8',
                'cinema_id' => $this->getCinemasById(),
                'capacity' => mt_rand(self::MIN_CAPACITY,self::MAX_CAPACITY)
            ]
        ]);
    }

    private function getCinemasById(){
        if(is_array($this->cinemaIDs)){
            foreach ($this->cinemaIDs as $cinemaID) {
                $this->usedCinemaIDs[] = $cinemaID->id;
                $indexes = array_keys($this->usedCinemaIDs, $cinemaID->id);
                if(count($indexes) <= 2){
                    return (int) $cinemaID->id;
                }
            }
        }
    }
}
