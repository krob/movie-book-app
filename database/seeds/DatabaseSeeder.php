<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CinemasTableSeeder::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(TheatresTableSeeder::class);
        $this->call(TheatresMoviesPivotTableSeeder::class);
    }
}
